/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_3;
    QLabel *status_Label;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label;
    QLineEdit *outputPath_LineEdit;
    QPushButton *selectPath_Button;
    QHBoxLayout *horizontalLayout;
    QPushButton *readlive_Button;
    QPushButton *readlog_Button;
    QPushButton *clear_Button;
    QHBoxLayout *horizontalLayout_2;
    QTextBrowser *outputData_TextBrowser;
    QGroupBox *Channels_GroupBox;
    QVBoxLayout *verticalLayout_2;
    QGroupBox *Modifiers_GroupBox;
    QVBoxLayout *verticalLayout_3;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(800, 600);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        status_Label = new QLabel(centralwidget);
        status_Label->setObjectName(QString::fromUtf8("status_Label"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(status_Label->sizePolicy().hasHeightForWidth());
        status_Label->setSizePolicy(sizePolicy1);

        horizontalLayout_3->addWidget(status_Label);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label = new QLabel(centralwidget);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy2(QSizePolicy::Maximum, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy2);
        label->setMinimumSize(QSize(0, 0));

        horizontalLayout_4->addWidget(label);

        outputPath_LineEdit = new QLineEdit(centralwidget);
        outputPath_LineEdit->setObjectName(QString::fromUtf8("outputPath_LineEdit"));

        horizontalLayout_4->addWidget(outputPath_LineEdit);

        selectPath_Button = new QPushButton(centralwidget);
        selectPath_Button->setObjectName(QString::fromUtf8("selectPath_Button"));
        QSizePolicy sizePolicy3(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(selectPath_Button->sizePolicy().hasHeightForWidth());
        selectPath_Button->setSizePolicy(sizePolicy3);
        selectPath_Button->setMaximumSize(QSize(20, 16777215));

        horizontalLayout_4->addWidget(selectPath_Button);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        readlive_Button = new QPushButton(centralwidget);
        readlive_Button->setObjectName(QString::fromUtf8("readlive_Button"));
        QSizePolicy sizePolicy4(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(readlive_Button->sizePolicy().hasHeightForWidth());
        readlive_Button->setSizePolicy(sizePolicy4);

        horizontalLayout->addWidget(readlive_Button);

        readlog_Button = new QPushButton(centralwidget);
        readlog_Button->setObjectName(QString::fromUtf8("readlog_Button"));

        horizontalLayout->addWidget(readlog_Button);

        clear_Button = new QPushButton(centralwidget);
        clear_Button->setObjectName(QString::fromUtf8("clear_Button"));
        QSizePolicy sizePolicy5(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy5.setHorizontalStretch(0);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(clear_Button->sizePolicy().hasHeightForWidth());
        clear_Button->setSizePolicy(sizePolicy5);

        horizontalLayout->addWidget(clear_Button);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        outputData_TextBrowser = new QTextBrowser(centralwidget);
        outputData_TextBrowser->setObjectName(QString::fromUtf8("outputData_TextBrowser"));
        QSizePolicy sizePolicy6(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy6.setHorizontalStretch(0);
        sizePolicy6.setVerticalStretch(0);
        sizePolicy6.setHeightForWidth(outputData_TextBrowser->sizePolicy().hasHeightForWidth());
        outputData_TextBrowser->setSizePolicy(sizePolicy6);

        horizontalLayout_2->addWidget(outputData_TextBrowser);

        Channels_GroupBox = new QGroupBox(centralwidget);
        Channels_GroupBox->setObjectName(QString::fromUtf8("Channels_GroupBox"));
        QSizePolicy sizePolicy7(QSizePolicy::Fixed, QSizePolicy::Minimum);
        sizePolicy7.setHorizontalStretch(0);
        sizePolicy7.setVerticalStretch(0);
        sizePolicy7.setHeightForWidth(Channels_GroupBox->sizePolicy().hasHeightForWidth());
        Channels_GroupBox->setSizePolicy(sizePolicy7);
        Channels_GroupBox->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        verticalLayout_2 = new QVBoxLayout(Channels_GroupBox);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));

        horizontalLayout_2->addWidget(Channels_GroupBox);

        Modifiers_GroupBox = new QGroupBox(centralwidget);
        Modifiers_GroupBox->setObjectName(QString::fromUtf8("Modifiers_GroupBox"));
        QSizePolicy sizePolicy8(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy8.setHorizontalStretch(0);
        sizePolicy8.setVerticalStretch(0);
        sizePolicy8.setHeightForWidth(Modifiers_GroupBox->sizePolicy().hasHeightForWidth());
        Modifiers_GroupBox->setSizePolicy(sizePolicy8);
        verticalLayout_3 = new QVBoxLayout(Modifiers_GroupBox);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));

        horizontalLayout_2->addWidget(Modifiers_GroupBox);


        verticalLayout->addLayout(horizontalLayout_2);

        MainWindow->setCentralWidget(centralwidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        status_Label->setText(QCoreApplication::translate("MainWindow", "Not Running.", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "Output Path:", nullptr));
        selectPath_Button->setText(QCoreApplication::translate("MainWindow", "..", nullptr));
        readlive_Button->setText(QCoreApplication::translate("MainWindow", "Read Live Data", nullptr));
        readlog_Button->setText(QCoreApplication::translate("MainWindow", "Read Log Data", nullptr));
        clear_Button->setText(QCoreApplication::translate("MainWindow", "Clear", nullptr));
        Channels_GroupBox->setTitle(QCoreApplication::translate("MainWindow", "Channels", nullptr));
        Modifiers_GroupBox->setTitle(QCoreApplication::translate("MainWindow", "Modifiers", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
