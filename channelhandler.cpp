#include "channelhandler.h"
#include <lcm/lcm-cpp.hpp>
#include "lcmOPAL2/header_t.hpp"
#include "lcmOPAL2/nav_data_msg_t.hpp"
#include "lcmOPAL2/nav_data_t.hpp"
#include "lcmOPAL2/point_data_msg_t.hpp"
#include "lcmOPAL2/point_data_raw_msg_t.hpp"
#include "lcmOPAL2/ins_data_msg_t.hpp"
#include "LCMHandler.h"
#include "QDebug"

//Nav data
void NavDataHandler::handleMsg(const lcm::ReceiveBuffer *rbuf, const std::string &chan)
{
    lcmOPAL2::nav_data_msg_t msg;
    msg.decode(rbuf->data, 0, rbuf->data_size);
    handle(msg);
}

void NavDataHandler::handleMsg(const lcm::LogEvent *rbuf, const std::string &chan)
{
    lcmOPAL2::nav_data_msg_t msg;
    msg.decode(rbuf->data, 0, rbuf->datalen);
    handle(msg);
}

void NavDataHandler::handle(lcmOPAL2::nav_data_msg_t &msg)
{
    handleMod(msg);

    lcm::LogEvent event;
    event.channel=m_channel.toStdString();
    event.timestamp=msg.header.timestamp;
    event.datalen=msg.getEncodedSize();
    event.data=malloc(event.datalen);
    msg.encode(event.data,0,event.datalen);
    m_mainHandler->logEvent(event);
    free(event.data);
    QString consoleLog = handleLog(msg);
    m_mainHandler->logConsole(consoleLog);
    QString csv=handleCSV(msg);
    m_mainHandler->logCSV(csv,m_channel);
}



QString NavDataHandler::handleCSV(lcmOPAL2::nav_data_msg_t &msg)
{
    QString str;
    str.append(QString::number(msg.header.timestamp) + ",");
    str.append(QString::number(msg.header.id) + ",");
    str.append(QString::fromStdString(msg.msg.frame) + ",");
    str.append(QString::fromStdString(msg.msg.description) + ",");
    str.append(QString::number(msg.msg.source) + ",");
    str.append(QString::number(msg.msg.pOrigin[0])
            + "," + QString::number(msg.msg.pOrigin[1])
            + "," + QString::number(msg.msg.pOrigin[2])
            + ",");
    str.append(QString::number(msg.msg.pXYZ[0])
            + "," + QString::number(msg.msg.pXYZ[1])
            + "," + QString::number(msg.msg.pXYZ[2])
            + ",");
    str.append(QString::number(msg.msg.qXYZW[0])
            + "," + QString::number(msg.msg.qXYZW[1])
            + "," + QString::number(msg.msg.qXYZW[2])
            + "," + QString::number(msg.msg.qXYZW[3])
            + ",");
    str.append(QString::number(msg.msg.vXYZ[0])
            + "," + QString::number(msg.msg.vXYZ[1])
            + "," + QString::number(msg.msg.vXYZ[2])
            + ",");
    str.append(QString::number(msg.msg.wXYZ[0])
            + "," + QString::number(msg.msg.wXYZ[1])
            + "," + QString::number(msg.msg.wXYZ[2])
            + ",");
    str.append(QString::number(msg.msg.aXYZ[0])
            + "," + QString::number(msg.msg.aXYZ[1])
            + "," + QString::number(msg.msg.aXYZ[2])
            + ",");
    str.append(QString::number(msg.msg.cov_size) + ",");
    str.append("\n");
    return str;
}

QString NavDataHandler::handleLog(lcmOPAL2::nav_data_msg_t &msg)
{
    QString logStr;
    logStr.append("Timestamp: " + QString::number(msg.header.timestamp) + "\n");
    logStr.append("Id: " + QString::number(msg.header.id) + "\n");
    logStr.append("Frame: " + QString::fromStdString(msg.msg.frame) + "\n");
    logStr.append("Description: " + QString::fromStdString(msg.msg.description) + "\n");
    logStr.append("Source: " + QString::number(msg.msg.source) + "\n");
    logStr.append("Origin: x:" + QString::number(msg.msg.pOrigin[0])
            + " y:" + QString::number(msg.msg.pOrigin[1])
            + " z:" + QString::number(msg.msg.pOrigin[2])
            + "\n");
    logStr.append("pXYZ: x:" + QString::number(msg.msg.pXYZ[0])
            + " y:" + QString::number(msg.msg.pXYZ[1])
            + " z:" + QString::number(msg.msg.pXYZ[2])
            + "\n");
    logStr.append("qXYZW: x:" + QString::number(msg.msg.qXYZW[0])
            + " y:" + QString::number(msg.msg.qXYZW[1])
            + " z:" + QString::number(msg.msg.qXYZW[2])
            + " w:" + QString::number(msg.msg.qXYZW[3])
            + "\n");
    logStr.append("vXYZ: x:" + QString::number(msg.msg.vXYZ[0])
            + " y:" + QString::number(msg.msg.vXYZ[1])
            + " z:" + QString::number(msg.msg.vXYZ[2])
            + "\n");
    logStr.append("wXYZ: x:" + QString::number(msg.msg.wXYZ[0])
            + " y:" + QString::number(msg.msg.wXYZ[1])
            + " z:" + QString::number(msg.msg.wXYZ[2])
            + "\n");
    logStr.append("aXYZ: x:" + QString::number(msg.msg.aXYZ[0])
            + " y:" + QString::number(msg.msg.aXYZ[1])
            + " z:" + QString::number(msg.msg.aXYZ[2])
            + "\n");
    logStr.append("COV size: " + QString::number(msg.msg.cov_size) + "\n");
    logStr.append("----------------------------------------\n");

    return logStr;
}

void NavDataHandler::handleMod(lcmOPAL2::nav_data_msg_t &msg)
{
    for(auto mod :m_mainHandler->mods()){
        switch (mod) {
        case LCMHandler::ModificationType::FeetToMeter:
            msg.msg.pOrigin[2] *= 0.3048;
            msg.msg.vXYZ[2] *= 0.3048;
            msg.msg.pXYZ[2] *= 0.3048;
            msg.msg.aXYZ[2] *= 0.3048;
            msg.msg.wXYZ[2] *= 0.3048;

            break;
        default:
            break;
        }
    }
}

//Point Data

void PointDataHandler::handleMsg(const lcm::ReceiveBuffer *rbuf, const std::string &chan)
{
    lcmOPAL2::point_data_msg_t msg;
    msg.decode(rbuf->data, 0, rbuf->data_size);
    handle(msg);
}

void PointDataHandler::handleMsg(const lcm::LogEvent *rbuf, const std::string &chan)
{
    lcmOPAL2::point_data_msg_t msg;
    msg.decode(rbuf->data, 0, rbuf->datalen);
    handle(msg);
}

void PointDataHandler::handle(lcmOPAL2::point_data_msg_t &msg)
{
    handleMod(msg);

    lcm::LogEvent event;
    event.channel=m_channel.toStdString();
    event.timestamp=msg.header.timestamp;
    event.datalen=msg.getEncodedSize();
    event.data=malloc(event.datalen);
    msg.encode(event.data,0,event.datalen);
    m_mainHandler->logEvent(event);
    free(event.data);
    QString consoleLog = handleLog(msg);
    m_mainHandler->logConsole(consoleLog);
    QString csv=handleCSV(msg);
    m_mainHandler->logCSV(csv,m_channel);
}

QString PointDataHandler::handleCSV(lcmOPAL2::point_data_msg_t &msg)
{
    QString str;
    str.append(QString::number(msg.header.timestamp) + ",");
    str.append(QString::number(msg.header.id) + ",");
    str.append(QString::number(msg.msg.sensorID) + ",");
    str.append(QString::fromStdString(msg.msg.uuid) + ",");
    str.append(QString::number(msg.msg.frameID) + ",");
    str.append( QString::fromStdString(msg.msg.reference_frame) + ",");
    str.append(QString::number(msg.msg.pOrigin[0])
            + "," + QString::number(msg.msg.pOrigin[1])
            + "," + QString::number(msg.msg.pOrigin[2])
            + ",");
    str.append(QString::fromStdString(msg.msg.info.source_frame) + ",");
    str.append(QString::fromStdString(msg.msg.info.target_frame) + ",");
    str.append(QString::number(msg.msg.info.pXYZ[0])
            + "," + QString::number(msg.msg.info.pXYZ[1])
            + "," + QString::number(msg.msg.info.pXYZ[2])
            + ",");
    str.append(QString::number(msg.msg.info.qXYZW[0])
            + "," + QString::number(msg.msg.info.qXYZW[1])
            + "," + QString::number(msg.msg.info.qXYZW[2])
            + "," + QString::number(msg.msg.info.qXYZW[3])
            + ",");
    str.append(QString::fromStdString(msg.msg.offset.source_frame) + ",");
    str.append(QString::fromStdString(msg.msg.offset.target_frame) + ",");
    str.append(QString::number(msg.msg.offset.pXYZ[0])
            + "," + QString::number(msg.msg.offset.pXYZ[1])
            + "," + QString::number(msg.msg.offset.pXYZ[2])
            + ",");
    str.append(QString::number(msg.msg.offset.qXYZW[0])
            + "," + QString::number(msg.msg.offset.qXYZW[1])
            + "," + QString::number(msg.msg.offset.qXYZW[2])
            + "," + QString::number(msg.msg.offset.qXYZW[3])
            + ",");
    str.append(QString::number(msg.msg.start) + ",");
    str.append(QString::number(msg.msg.duration) + ",");
    str.append(QString::number(msg.msg.num_points) + ",");
    for(int i =0;i< msg.msg.num_points;i++){
        str.append("Point"+QString::number(i)+" x:" + QString::number(msg.msg.points[i].X)
                      + " y:" + QString::number(msg.msg.points[i].Y)
                      + " z:" + QString::number(msg.msg.points[i].Z)
                      +" Timsestamp:" + QString::number(msg.msg.points[i].timestamp)
                      +" Intensity:" + QString::number(msg.msg.points[i].intensity)+",");
    }
    str.append("\n");
    return str;
}

QString PointDataHandler::handleLog(lcmOPAL2::point_data_msg_t &msg)
{
    QString logStr;
    logStr.append("Timestamp: " + QString::number(msg.header.timestamp) + "\n");
    logStr.append("Id: " + QString::number(msg.header.id) + "\n");
    logStr.append("sensorID: " + QString::number(msg.msg.sensorID) + "\n");
    logStr.append("uuid: " + QString::fromStdString(msg.msg.uuid) + "\n");
    logStr.append("Frame: " + QString::number(msg.msg.frameID) + "\n");
    logStr.append("Reference Frame: " + QString::fromStdString(msg.msg.reference_frame) + "\n");
    logStr.append("Origin: x:" + QString::number(msg.msg.pOrigin[0])
            + " y:" + QString::number(msg.msg.pOrigin[1])
            + " z:" + QString::number(msg.msg.pOrigin[2])
            + "\n");
    logStr.append("Info Source Frame: " + QString::fromStdString(msg.msg.info.source_frame) + "\n");
    logStr.append("Info Target Frame: " + QString::fromStdString(msg.msg.info.target_frame) + "\n");
    logStr.append("Info pXYZ: x:" + QString::number(msg.msg.info.pXYZ[0])
            + " y:" + QString::number(msg.msg.info.pXYZ[1])
            + " z:" + QString::number(msg.msg.info.pXYZ[2])
            + "\n");
    logStr.append("Info qXYZW: x:" + QString::number(msg.msg.info.qXYZW[0])
            + " y:" + QString::number(msg.msg.info.qXYZW[1])
            + " z:" + QString::number(msg.msg.info.qXYZW[2])
            + " w:" + QString::number(msg.msg.info.qXYZW[3])
            + "\n");
    logStr.append("Offset Source Frame: " + QString::fromStdString(msg.msg.offset.source_frame) + "\n");
    logStr.append("Offset Target Frame: " + QString::fromStdString(msg.msg.offset.target_frame) + "\n");
    logStr.append("Offset pXYZ: x:" + QString::number(msg.msg.offset.pXYZ[0])
            + " y:" + QString::number(msg.msg.offset.pXYZ[1])
            + " z:" + QString::number(msg.msg.offset.pXYZ[2])
            + "\n");
    logStr.append("Offset qXYZW: x:" + QString::number(msg.msg.offset.qXYZW[0])
            + " y:" + QString::number(msg.msg.offset.qXYZW[1])
            + " z:" + QString::number(msg.msg.offset.qXYZW[2])
            + " w:" + QString::number(msg.msg.offset.qXYZW[3])
            + "\n");
    logStr.append("Scan Start: " + QString::number(msg.msg.start) + "\n");
    logStr.append("Scan Duration: " + QString::number(msg.msg.duration) + "\n");
    logStr.append("Num Points: " + QString::number(msg.msg.num_points) + "\n");
//    for(int i =0;i< msg.msg.num_points;i++){
//        logStr.append("Point"+QString::number(i)+"x: " + QString::number(msg.msg.points[i].X)
//                      + " y: " + QString::number(msg.msg.points[i].Y)
//                      + " z: " + QString::number(msg.msg.points[i].Z)
//                      + "\n"
//                      +"Timsestamp: " + QString::number(msg.msg.points[i].timestamp)
//                      +"Intensity: " + QString::number(msg.msg.points[i].intensity));
//    }
    logStr.append("----------------------------------------\n");

    return logStr;
    //qDebug()<<logStr;
}

void PointDataHandler::handleMod(lcmOPAL2::point_data_msg_t &msg)
{

}

//Raw Point Data


void PointDataRawHandler::handleMsg(const lcm::ReceiveBuffer *rbuf, const std::string &chan)
{
    lcmOPAL2::point_data_raw_msg_t msg;
    msg.decode(rbuf->data, 0, rbuf->data_size);
    handle(msg);
}

void PointDataRawHandler::handleMsg(const lcm::LogEvent *rbuf, const std::string &chan)
{
    lcmOPAL2::point_data_raw_msg_t msg;
    msg.decode(rbuf->data, 0, rbuf->datalen);
    handle(msg);
}

void PointDataRawHandler::handle(lcmOPAL2::point_data_raw_msg_t &msg)
{
    handleMod(msg);

    lcm::LogEvent event;
    event.channel=m_channel.toStdString();
    event.timestamp=msg.header.timestamp;
    event.datalen=msg.getEncodedSize();
    event.data=malloc(event.datalen);
    msg.encode(event.data,0,event.datalen);
    m_mainHandler->logEvent(event);
    free(event.data);
    QString consoleLog = handleLog(msg);
    m_mainHandler->logConsole(consoleLog);
    QString csv=handleCSV(msg);
    m_mainHandler->logCSV(csv,m_channel);
}


QString PointDataRawHandler::handleCSV(lcmOPAL2::point_data_raw_msg_t &msg)
{
    QString str;
    str.append(QString::number(msg.header.timestamp) + ",");
    str.append(QString::number(msg.header.id) + ",");
    str.append(QString::number(msg.msg.sensorID) + ",");
    str.append(QString::fromStdString(msg.msg.uuid) + ",");
    str.append(QString::number(msg.msg.pOrigin[0])
            + "," + QString::number(msg.msg.pOrigin[1])
            + "," + QString::number(msg.msg.pOrigin[2])
            + ",");
    str.append(QString::fromStdString(msg.msg.info.source_frame) + ",");
    str.append(QString::fromStdString(msg.msg.info.target_frame) + ",");
    str.append(QString::number(msg.msg.info.pXYZ[0])
            + "," + QString::number(msg.msg.info.pXYZ[1])
            + "," + QString::number(msg.msg.info.pXYZ[2])
            + ",");
    str.append(QString::number(msg.msg.info.qXYZW[0])
            + "," + QString::number(msg.msg.info.qXYZW[1])
            + "," + QString::number(msg.msg.info.qXYZW[2])
            + "," + QString::number(msg.msg.info.qXYZW[3])
            + ",");
    str.append(QString::fromStdString(msg.msg.offset.source_frame) + ",");
    str.append(QString::fromStdString(msg.msg.offset.target_frame) + ",");
    str.append(QString::number(msg.msg.offset.pXYZ[0])
            + "," + QString::number(msg.msg.offset.pXYZ[1])
            + "," + QString::number(msg.msg.offset.pXYZ[2])
            + ",");
    str.append(QString::number(msg.msg.offset.qXYZW[0])
            + "," + QString::number(msg.msg.offset.qXYZW[1])
            + "," + QString::number(msg.msg.offset.qXYZW[2])
            + "," + QString::number(msg.msg.offset.qXYZW[3])
            + ",");
    str.append(QString::number(msg.msg.start) + ",");
    str.append(QString::number(msg.msg.duration) + ",");
    str.append(QString::number(msg.msg.num_points) + ",");
    for(int i =0;i< msg.msg.num_points;i++){
        str.append("Point"+QString::number(i)+" x:" + QString::number(msg.msg.points[i].X)
                      + " y:" + QString::number(msg.msg.points[i].Y)
                      + " z:" + QString::number(msg.msg.points[i].Z)
                      +" Timsestamp:" + QString::number(msg.msg.points[i].timestamp)
                      +" Intensity:" + QString::number(msg.msg.points[i].intensity)+",");
    }
    for(int i =0;i< msg.msg.num_points;i++){
        str.append("Point"+QString::number(i)+" u:" + QString::number(msg.msg.raw_points[i].U)
                      + " v:" + QString::number(msg.msg.raw_points[i].V)
                      + " r:" + QString::number(msg.msg.raw_points[i].R)
                      +" Status:" + QString::number(msg.msg.raw_points[i].status)
                      +" Intensity:" + QString::number(msg.msg.raw_points[i].intensity)
                      +" LeftSide:" + QString::number(msg.msg.raw_points[i].leftSide)
                      +" EdgeID:" + QString::number(msg.msg.raw_points[i].edgeId)
                           +",");
    }
    str.append("\n");
    return str;
}

QString PointDataRawHandler::handleLog(lcmOPAL2::point_data_raw_msg_t &msg)
{
    QString logStr;
    logStr.append("Timestamp: " + QString::number(msg.header.timestamp) + "\n");
    logStr.append("Id: " + QString::number(msg.header.id) + "\n");
    logStr.append("sensorID: " + QString::number(msg.msg.sensorID) + "\n");
    logStr.append("uuid: " + QString::fromStdString(msg.msg.uuid) + "\n");
    logStr.append("Origin: x:" + QString::number(msg.msg.pOrigin[0])
            + " y:" + QString::number(msg.msg.pOrigin[1])
            + " z:" + QString::number(msg.msg.pOrigin[2])
            + "\n");
    logStr.append("Info Source Frame: " + QString::fromStdString(msg.msg.info.source_frame) + "\n");
    logStr.append("Info Target Frame: " + QString::fromStdString(msg.msg.info.target_frame) + "\n");
    logStr.append("Info pXYZ: x:" + QString::number(msg.msg.info.pXYZ[0])
            + " y:" + QString::number(msg.msg.info.pXYZ[1])
            + " z:" + QString::number(msg.msg.info.pXYZ[2])
            + "\n");
    logStr.append("Info qXYZW: x:" + QString::number(msg.msg.info.qXYZW[0])
            + " y:" + QString::number(msg.msg.info.qXYZW[1])
            + " z:" + QString::number(msg.msg.info.qXYZW[2])
            + " w:" + QString::number(msg.msg.info.qXYZW[3])
            + "\n");
    logStr.append("Offset Source Frame: " + QString::fromStdString(msg.msg.offset.source_frame) + "\n");
    logStr.append("Offset Target Frame: " + QString::fromStdString(msg.msg.offset.target_frame) + "\n");
    logStr.append("Offset pXYZ: x:" + QString::number(msg.msg.offset.pXYZ[0])
            + " y:" + QString::number(msg.msg.offset.pXYZ[1])
            + " z:" + QString::number(msg.msg.offset.pXYZ[2])
            + "\n");
    logStr.append("Offset qXYZW: x:" + QString::number(msg.msg.offset.qXYZW[0])
            + " y:" + QString::number(msg.msg.offset.qXYZW[1])
            + " z:" + QString::number(msg.msg.offset.qXYZW[2])
            + " w:" + QString::number(msg.msg.offset.qXYZW[3])
            + "\n");
    logStr.append("Scan Start: " + QString::number(msg.msg.start) + "\n");
    logStr.append("Scan Duration: " + QString::number(msg.msg.duration) + "\n");
    logStr.append("Num Points: " + QString::number(msg.msg.num_points) + "\n");
    logStr.append("----------------------------------------\n");

    return logStr;
    //qDebug()<<logStr;
}

void PointDataRawHandler::handleMod(lcmOPAL2::point_data_raw_msg_t &msg)
{

}
// Ins Data

void InsDataHandler::handleMsg(const lcm::ReceiveBuffer *rbuf, const std::string &chan)
{
    lcmOPAL2::ins_data_msg_t msg;
    msg.decode(rbuf->data, 0, rbuf->data_size);
    handle(msg);
}

void InsDataHandler::handleMsg(const lcm::LogEvent *rbuf, const std::string &chan)
{
    lcmOPAL2::ins_data_msg_t msg;
    msg.decode(rbuf->data, 0, rbuf->datalen);
    handle(msg);
}

void InsDataHandler::handle(lcmOPAL2::ins_data_msg_t &msg)
{
    handleMod(msg);

    lcm::LogEvent event;
    event.channel=m_channel.toStdString();
    event.timestamp=msg.header.timestamp;
    event.datalen=msg.getEncodedSize();
    event.data=malloc(event.datalen);
    msg.encode(event.data,0,event.datalen);
    m_mainHandler->logEvent(event);
    free(event.data);
    QString consoleLog = handleLog(msg);
    m_mainHandler->logConsole(consoleLog);
    QString csv=handleCSV(msg);
    m_mainHandler->logCSV(csv,m_channel);
}


QString InsDataHandler::handleCSV(lcmOPAL2::ins_data_msg_t &msg)
{
    QString str;
    str.append(QString::number(msg.header.timestamp) + ",");
    str.append(QString::number(msg.header.id) + ",");
    str.append(QString::number(msg.msg.insID) + ",");
    str.append(QString::number(msg.msg.timeSinceEpoch) + ",");
    str.append(QString::number(msg.msg.dLatitude) + ",");
    str.append(QString::number(msg.msg.dLongitude) + ",");
    str.append(QString::number(msg.msg.dAltitude) + ",");
    str.append(QString::number(msg.msg.fNorthVelocity) + ",");
    str.append(QString::number(msg.msg.fEastVelocity) + ",");
    str.append(QString::number(msg.msg.fDownVelocity) + ",");
    str.append(QString::number(msg.msg.dVehicleRoll) + ",");
    str.append(QString::number(msg.msg.dVehiclePitch) + ",");
    str.append(QString::number(msg.msg.dVehicleHeading) + ",");
    str.append(QString::number(msg.msg.dVehicleWanderAngle) + ",");
    str.append(QString::number(msg.msg.fVehicleTrackAngle) + ",");
    str.append(QString::number(msg.msg.fVehicleSpeed) + ",");
    str.append(QString::number(msg.msg.fVehicleAngularRateLongitudinalAxis) + ",");
    str.append(QString::number(msg.msg.fVehicleAngularRateTransverseAxis) + ",");
    str.append(QString::number(msg.msg.fVehicleAngularRateDownAxis) + ",");
    str.append(QString::number(msg.msg.fVehicleLongitudinalAcceleration) + ",");
    str.append(QString::number(msg.msg.fVehicleTransverseAcceleration) + ",");
    str.append(QString::number(msg.msg.fVehicleDownAcceleration) + ",");
    str.append(QString::number(msg.msg.byAlignmentStatus) + ",");


    str.append("\n");
    return str;
}

QString InsDataHandler::handleLog(lcmOPAL2::ins_data_msg_t &msg)
{
    QString logStr;

    logStr.append("TimeStamp:"+QString::number(msg.header.timestamp) + "\n");
    logStr.append("ID:"+QString::number(msg.header.id) + "\n");
    logStr.append("Ins ID:"+QString::number(msg.msg.insID) + "\n");
    logStr.append("GPS Time:"+QString::number(msg.msg.timeSinceEpoch) + "\n");
    logStr.append("Latitude:"+QString::number(msg.msg.dLatitude) + "\n");
    logStr.append("Longitude:"+QString::number(msg.msg.dLongitude) + "\n");
    logStr.append("Altitude:"+QString::number(msg.msg.dAltitude) + "\n");
    logStr.append("North Velocity:"+QString::number(msg.msg.fNorthVelocity) + "\n");
    logStr.append("East Velocity:"+QString::number(msg.msg.fEastVelocity) + "\n");
    logStr.append("Down Velocity:"+QString::number(msg.msg.fDownVelocity) + "\n");
    logStr.append("Roll:"+QString::number(msg.msg.dVehicleRoll) + "\n");
    logStr.append("Pitch:"+QString::number(msg.msg.dVehiclePitch) + "\n");
    logStr.append("Heading:"+QString::number(msg.msg.dVehicleHeading) + "\n");
    logStr.append("Wander Angle:"+QString::number(msg.msg.dVehicleWanderAngle) + "\n");
    logStr.append("Track Angle:"+QString::number(msg.msg.fVehicleTrackAngle) + "\n");
    logStr.append("Speed:"+QString::number(msg.msg.fVehicleSpeed) + "\n");
    logStr.append("Angular Rate Longitudinal Axis:"+QString::number(msg.msg.fVehicleAngularRateLongitudinalAxis) + "\n");
    logStr.append("Angular Rate Transverse Axis:"+QString::number(msg.msg.fVehicleAngularRateTransverseAxis) + "\n");
    logStr.append("Angular Rate Down Axis:"+QString::number(msg.msg.fVehicleAngularRateDownAxis) + "\n");
    logStr.append("Longitudinal Acceleration:"+QString::number(msg.msg.fVehicleLongitudinalAcceleration) + "\n");
    logStr.append("Transverse Acceleration:"+QString::number(msg.msg.fVehicleTransverseAcceleration) + "\n");
    logStr.append("Down Acceleration:"+QString::number(msg.msg.fVehicleDownAcceleration) + "\n");
    logStr.append("Alignment Status:"+QString::number(msg.msg.byAlignmentStatus) + "\n");
    logStr.append("----------------------------------------\n");

    return logStr;
    //qDebug()<<logStr;
}

void InsDataHandler::handleMod(lcmOPAL2::ins_data_msg_t &msg)
{
    for(auto mod :m_mainHandler->mods()){
        switch (mod) {
        case LCMHandler::ModificationType::FeetToMeter:
            msg.msg.dAltitude *= 0.3048;

            break;
        default:
            break;
        }
    }
}
