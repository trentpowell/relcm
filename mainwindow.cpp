#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "lcmOPAL2/header_t.hpp"
#include "lcmOPAL2/nav_data_msg_t.hpp"
#include "lcmOPAL2/point_data_msg_t.hpp"
#include "lcmOPAL2/point_data_raw_msg_t.hpp"
#include "lcmOPAL2/point_data_t.hpp"
#include "LCMHandler.h"
#include <QFileDialog>
#include <LCMHandler.h>
#include <QMessageBox>
#include <QTextStream>
#include <QDebug>
#include <QCheckBox>

MainWindow::MainWindow(QWidget *parent, lcm::LCM* lcm,LCMHandler* handler)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , m_lcm(lcm)
    , m_handler(handler)
{
    ui->setupUi(this);
    m_running=false;
    ui->Channels_GroupBox->layout()->setAlignment(Qt::AlignTop);
    ui->Modifiers_GroupBox->layout()->setAlignment(Qt::AlignTop);
//adding checkbox for each channel
    for(auto chan:*m_handler->channels()){
        QCheckBox *cb = new QCheckBox(chan->channel());
        connect(cb,&QCheckBox::toggled,this,[=](bool value){
            value ? m_handler->activeChannels()->append(chan): (void) m_handler->activeChannels()->removeOne(chan);
        });
        ui->Channels_GroupBox->layout()->addWidget(cb);
    }
//modifications
    //temp
    m_consoleEnabled=false;
    QCheckBox *consolecb = new QCheckBox("Enable Console");
    ui->Modifiers_GroupBox->layout()->addWidget(consolecb);
    connect(consolecb,&QCheckBox::toggled,this,[=](bool value){
        m_consoleEnabled=value;
    });

    //this can be more elegant with more mods
    QCheckBox *cb = new QCheckBox("FeetToMeter");
    ui->Modifiers_GroupBox->layout()->addWidget(cb);
    connect(cb,&QCheckBox::toggled,this,[=](bool value){
        m_handler->toggleMod(LCMHandler::FeetToMeter);
    });

//other connections
    connect(ui->readlive_Button,&QPushButton::clicked,this,&MainWindow::toggleState);
    connect(m_handler,&LCMHandler::logReady,this,&MainWindow::logData);
    connect(ui->outputPath_LineEdit,&QLineEdit::editingFinished,this,[=](){
    m_handler->prepareFiles(ui->outputPath_LineEdit->text(),*m_handler->channels());
    });
    connect(ui->clear_Button,&QPushButton::pressed,this,&MainWindow::clear);
    connect(ui->readlog_Button,&QPushButton::pressed,this,&MainWindow::readLog);
    connect(ui->selectPath_Button,&QPushButton::pressed,this,&MainWindow::setPath);

    setWindowTitle(QString("Opal LCM Reader"));
    logMsg("Welcome! Please select desired channels. Then select an output path.");
}

MainWindow::~MainWindow()
{
    delete ui;
    delete m_lcm;
    delete m_handler;

}

void MainWindow::logData(const QString* msg)
{
    m_msgCount++;
    if(m_consoleEnabled){
        try {

            ui->outputData_TextBrowser->append(*msg);
        }  catch (_exception e) {
            qDebug()<<"CONSOLE UPDATING TOO FAST ERROR";
        }
    }
    else{
         ui->status_Label->setText("Reading channels... Msg Count:"+QString::number(m_msgCount));
    }
}

void MainWindow::logMsg(QString msg)
{
    ui->outputData_TextBrowser->append(msg);
}

void MainWindow::setPath()
{
    QString path = QFileDialog::getExistingDirectory(this,"Save files in..","/..");
    ui->outputPath_LineEdit->setText(path);
    logMsg(m_handler->prepareFiles(path,*m_handler->activeChannels()));
}

void MainWindow::clear()
{
    ui->outputData_TextBrowser->clear();
    m_handler->prepareFiles(ui->outputPath_LineEdit->text(),*m_handler->activeChannels());
    m_msgCount=0;
}

void MainWindow::toggleState()
{
    if(m_running){//disable
        ui->status_Label->setText("Not Running.");
        ui->readlive_Button->setText("Read Live Data");
        setWindowTitle(QString("Opal LCM Reader"));
        for (auto sub :m_currentSubscriptions ) {
            m_lcm->unsubscribe(sub);
        }
        m_currentSubscriptions.clear();
        m_running =false;
    }else{//enable
        ui->status_Label->setText("Reading channels...");
        ui->readlive_Button->setText("Stop");
        for (auto chan :*m_handler->channels() ) {
            if(m_handler->activeChannels()->contains(chan)){
                m_currentSubscriptions.append(m_lcm->subscribe(chan->channel().toStdString(), &ChannelHandler<void*>::handleMsg,chan));
            }
        }
        m_running =true;
    }
}

void MainWindow::readLog()
{
    QString path = QFileDialog::getOpenFileName(nullptr,"Open Log File","/..");
    if(path=="")return;

    QThread *workerThread = new QThread();
    LogReader *reader = new LogReader(this,m_handler,path);
    reader->moveToThread(workerThread);
    connect(workerThread,&QThread::started,reader,&LogReader::read);
    connect(reader, &LogReader::doneReading, workerThread, &QThread::quit);
    connect(workerThread,&QThread::finished,reader,&QObject::deleteLater);
    connect(reader, &LogReader::doneReading,this,[=](){
        logMsg("Done Reading!");
    });
    workerThread->start();

}

void MainWindow::checkLCM()
{

}



void LogReader::read(){
    lcm::LogFile file(m_path.toStdString(),"r");
    if(!file.good()){
        QMessageBox::warning(m_parent,"Error", "Error");
        return;
    }
    while(true){
        const lcm::LogEvent* currentEvent = file.readNextEvent();
        if (currentEvent == nullptr){emit doneReading();return;}
        for(auto chan : *m_lcmHandler->activeChannels()){
            if(currentEvent->channel==chan->channel().toStdString()){
                chan->handleMsg(currentEvent,chan->channel().toStdString());
                this->thread()->usleep(1000/30);
            }

        }
    }
}
