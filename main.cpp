//Created by Trent Powell
#include "mainwindow.h"
#include <lcm/lcm-cpp.hpp>
#include <QApplication>
#include "LCMHandler.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

//LCM SETUP
    lcm::LCM *lcm = new lcm::LCM();
    if (!lcm->good())//test lcm
    {
        qDebug()<<"LCM NOT FOUND";
    }
    qDebug()<<"LCM FOUND";
//HANDLER SETUP
    QScopedPointer<QThread> handlerThread(new QThread());//put handler on its own thread
    QScopedPointer<LCMHandler> handler(new LCMHandler(lcm));
    handler->moveToThread(handlerThread.data());
//CHANNEL SETUP
//add new channels here
    handler->channels()->append((ChannelHandler<void*>*) new NavDataHandler("OPAL2_NavData_INS_ExternalNavConverter",handler.data()));
    handler->channels()->append((ChannelHandler<void*>*) new PointDataHandler("OPAL2_Register_Points",handler.data()));
    handler->channels()->append((ChannelHandler<void*>*) new PointDataHandler("OPAL2_SensorManager_Points1128",handler.data()));
    handler->channels()->append((ChannelHandler<void*>*) new InsDataHandler("OPAL2_3DRiClient_Nav_Data",handler.data()));

//WINDOW SETUP
    MainWindow w(nullptr,lcm,handler.data());
    w.show();
// START THE THREAD

    QObject::connect(handlerThread.data(),&QThread::started,handler.data(),&LCMHandler::handle);
    QObject::connect(handlerThread.data(),&QThread::finished,handlerThread.data(),&QThread::deleteLater);
    handlerThread->start();
    return a.exec();
}
