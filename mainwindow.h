#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <channelhandler.h>
#include <qthread.h>
#include <lcm/lcm-cpp.hpp>
#include <QDebug>
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

namespace lcm{class LCM;class Subscription;}

class LCMHandler;
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr, lcm::LCM* lcm=nullptr,LCMHandler* handler=nullptr);
    ~MainWindow();
    void logData(const QString* msg);
    QString logPath();
    QString csvPath();
    void logMsg(QString msg);
private:
    void checkLCM();
    void saveCSV();
    void setPath();
    void clear();

private:
    Ui::MainWindow *ui;
    lcm::LCM *m_lcm;
    LCMHandler* m_handler;
    bool m_running;
    bool m_consoleEnabled;
    int m_msgCount;
    QVector<lcm::Subscription*> m_currentSubscriptions;

public slots:
    void toggleState();
    void readLog();
};
/*
 * this class is awful and should be reworked
 * The logreading shouldnt have dependancies
 */
class LogReader: public QObject
{
    Q_OBJECT
private:
    LCMHandler* m_lcmHandler;
    QString m_path;
    QWidget* m_parent;
public:
    LogReader(QWidget *parent =nullptr,LCMHandler *handler=nullptr,QString path = ""):m_lcmHandler(handler),m_path(path),m_parent(parent){}

    void read();
signals:
    void doneReading();
};
#endif // MAINWINDOW_H
