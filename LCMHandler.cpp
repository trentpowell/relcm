#include <lcm/lcm-cpp.hpp>
#include <QDateTime>
#include "channelhandler.h"
#include "LCMHandler.h"
#include "QDebug"
#include "QTemporaryFile"
LCMHandler::LCMHandler(lcm::LCM* lcm):m_lcm(lcm)
{
}

LCMHandler::~LCMHandler()
{
    delete m_logFile;
    m_csvFiles.clear();
}

void LCMHandler::toggleMod(ModificationType mod)
{
    if(m_mods.contains(mod)){
        m_mods.removeOne(mod);
    }else{
        m_mods.append(mod);
    }
}

QVector<LCMHandler::ModificationType> LCMHandler::mods()
{
    return m_mods;
}

void LCMHandler::logEvent(lcm::LogEvent &event)
{
    m_logFile->writeEvent(&event);
}

void LCMHandler::logCSV(QString &line, QString &channel)
{
    for(auto &file :m_csvFiles){
        if(file->fileName().contains(channel)){
            file->write(line.toUtf8());
        }
    }
}

void LCMHandler::logConsole(QString &str)
{
    emit logReady(&str);
}

QString LCMHandler::prepareFiles(QString path, QVector<ChannelHandler<void*>*> &channels)
{
    if(path==m_outputPath)return "Path did not change.";
    QString filename = path.append("/").append(QString::number(QDateTime::currentMSecsSinceEpoch()));

    m_logFile = new lcm::LogFile((filename+".logfile").toStdString(),"w");//log file creation
    if(!m_logFile->good()) return "Error creating logfile";

    m_csvFiles.clear();
    for(auto &chan : channels){//csv creation
        QFile *f = new QFile(filename+chan->channel()+".csv");
        QTextStream out(f);
        out<<chan->csvHeaders();
        f->open(QIODevice::WriteOnly);
        m_csvFiles.append(f);
    }
    return "Path successfuly changed to "+path;
}

void LCMHandler::handle()
{
    while (0 == m_lcm->handle()) {

        }
    qDebug()<<"broken";
}
