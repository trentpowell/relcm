# ReLCM

[Basic Description](#desc)
[Deatiled Build Instructions](#build)
[Detailed Explaination](#detail)

# Description {#desc}

ReLCM is a an [LCM](https://lcm-proj.github.io/index.html) reader and recorder. It can read both from live lcm messages and lcmlog files. Current outputs are CSV and lcmlog formats. Desired LCM channels must be manually added with defined outputs. The currently included Opal channels are the following:
- OPAL2_NavData_INS_ExternalNavConverter
- OPAL2_Register_Points
- OPAL2_SensorManager_Points1128
- OPAL2_3DRiClient_Nav_Data

The inputted data can be modified per channel. Only current modification is feet to meters.

## Requirements
- [LCM](https://lcm-proj.github.io/index.html)
### To Build
- [Visual Studio 2019](https://visualstudio.microsoft.com/downloads/)
- [Qt](https://www.qt.io/)

## How to build
**Windows**
```
cd /to/install/path
qmake
nmake
```

### Expanding

Currently new channels and modifications must be added manually. To add a new channel you need its lcm definition in c++.
To do this run lcm-gen: lcm-gen ```-x example_t.lcm``` 
Added channels should inherit from ChannelHandler. They then need to also be added in the main file under channel setup. 

```
    handler->channels()->append((ChannelHandler<void*>*) new YOURCLASSHERE("CHANNEL_NAME_HERE",handler.data()));
```
Examples for creating channelhandler subclasses can be seen in the existing opal subclasses.


## Future Improvements
1. Channel adding should be automatic meaning the generation of c++ code from the lcm file.
2. Redo UI

# Detailed Windows Build Instructions {#build}

## Install LCM

Before you install LCM on Windows you need to download and install [CMake](https://cmake.org/download/), [MSYS2](https://www.msys2.org/) for GTK, and [Visual Studio](https://visualstudio.microsoft.com/downloads/). 
- The CMake install is very easy. Just download and run the cmake msi and making sure that you check the button to add cmake to your path.
- MSYS2 is more complicated but you don't need to do much with it.
	- Go to the [MSYS2](https://www.msys2.org/) site and download the exe
	- Go through the installer selecting default values.
	- Open MSYS2 and paste the following line. This installs gtk.
		'''pacman -S mingw-w64-x86_64-gtk3'''
	- It might take a while to download but press Y when prompted.
	- You can close MSYS2 when finished
- Visual Studio can be downloaded without any development kits if you like. Follow the installer.
Now we can install lcm. Download the [binaries](https://github.com/lcm-proj/lcm/releases) and extract them.
C:/msys64/mingw64/lib/glib-2.0/include
C:/msys64/mingw64/include/glib-2.0/
C:/msys64/mingw64/lib/libglib-2.0.dll.a
C:/msys64/mingw64/bin/libglib-2.0.dll
# Detailed Explaination {#detail}

Todo 
