#ifndef CHANNELHANDLER_H
#define CHANNELHANDLER_H

#include <QString>
//My lack of C++ knowledge forces this solution to be jank. How do I use inheritance correctly here?

namespace lcm{class LCM;class ReceiveBuffer;class LogEvent;}
namespace lcmOPAL2{class nav_data_msg_t;class point_data_msg_t;class point_data_raw_msg_t;class ins_data_msg_t;}
class LCMHandler;
template<typename T>
class ChannelHandler
{
public:
    ChannelHandler(LCMHandler* handler,QString channel):m_mainHandler(handler),m_channel(channel){}
    virtual void handleMsg(const lcm::ReceiveBuffer* rbuf,const std::string& chan)=0;
    virtual void handleMsg(const lcm::LogEvent* rbuf,const std::string& chan)=0;
    virtual void handle(T& data)=0;
    QString channel(){return m_channel;}
    virtual QString csvHeaders(){return m_csvHeaders;}
protected:
    virtual QString handleCSV(T& msg)=0;
    virtual QString handleLog(T& msg)=0;
    virtual void handleMod(T& msg)=0;
protected:
    LCMHandler* m_mainHandler;
    QString m_channel;
    QString m_csvHeaders;
};


class NavDataHandler: public ChannelHandler<lcmOPAL2::nav_data_msg_t>
{
public:
    NavDataHandler(QString channel,LCMHandler* handler):ChannelHandler(handler,channel){
        m_csvHeaders="Timestamp,Id,Frame,Description,Source,Origin_x,Origin_y,Origin_z,p_X,p_Y,p_Z,q_X,q_Y,q_Z,q_W,v_X,v_Y,v_Z,w_X,w_Y,w_Z,a_X,a_Y,a_Z,COV size\n";
    }
    void handleMsg(const lcm::ReceiveBuffer* rbuf,const std::string& chan) override;
    void handleMsg(const lcm::LogEvent* rbuf,const std::string& chan) override;
    void handle(lcmOPAL2::nav_data_msg_t& data) override;



protected:
    QString handleCSV(lcmOPAL2::nav_data_msg_t& msg) override;
    QString handleLog(lcmOPAL2::nav_data_msg_t& msg) override;
    void handleMod(lcmOPAL2::nav_data_msg_t& msg) override;
};

class PointDataHandler: public ChannelHandler<lcmOPAL2::point_data_msg_t>
{
public:
    PointDataHandler(QString channel,LCMHandler* handler):ChannelHandler(handler,channel){
        m_csvHeaders="Timestamp,Id,sensorID,uuid,FrameID,Reference Frame,Origin_x,Origin_y,Origin_z,Info Source Frame,Info Target Frame,Info p_X,Info p_Y,Info p_Z,"
                     "Info q_X,Info q_Y,Info q_Z,Info q_W,Offset Source Frame,Offset Target Frame,Offset p_X,Offset p_Y,Offset p_Z,Offset q_X,Offset q_Y,Offset q_Z,Offset q_W,Scan Start,Scan Duration,Points""\n";
    }

    void handleMsg(const lcm::ReceiveBuffer* rbuf,const std::string& chan) override;
    void handleMsg(const lcm::LogEvent* rbuf,const std::string& chan) override;
    void handle(lcmOPAL2::point_data_msg_t& data) override;


protected:
    QString handleCSV(lcmOPAL2::point_data_msg_t& msg) override;
    QString handleLog(lcmOPAL2::point_data_msg_t& msg) override;
    void handleMod(lcmOPAL2::point_data_msg_t& msg) override;
};

class PointDataRawHandler: public ChannelHandler<lcmOPAL2::point_data_raw_msg_t>
{
public:
    PointDataRawHandler(QString channel,LCMHandler* handler):ChannelHandler(handler,channel){
        m_csvHeaders="Timestamp,Id,sensorID,uuid,Origin_x,Origin_y,Origin_z,Info Source Frame,Info Target Frame,Info p_X,Info p_Y,Info p_Z,"
                                       "Info q_X,Info q_Y,Info q_Z,Info q_W,Offset Source Frame,Offset Target Frame,Offset p_X,Offset p_Y,Offset p_Z,Offset q_X,Offset q_Y,Offset q_Z,Offset q_W,Scan Start,Scan Duration,Points""\n";
    }

    void handleMsg(const lcm::ReceiveBuffer* rbuf,const std::string& chan) override;
    void handleMsg(const lcm::LogEvent* rbuf,const std::string& chan) override;
    void handle(lcmOPAL2::point_data_raw_msg_t& data) override;

protected:
    QString handleCSV(lcmOPAL2::point_data_raw_msg_t& msg) override;
    QString handleLog(lcmOPAL2::point_data_raw_msg_t& msg) override;
    void handleMod(lcmOPAL2::point_data_raw_msg_t& msg) override;
};

class InsDataHandler: public ChannelHandler<lcmOPAL2::ins_data_msg_t>
{
public:
    InsDataHandler(QString channel,LCMHandler* handler):ChannelHandler(handler,channel){
        m_csvHeaders = "TimeStamp,ID,Ins ID,GPS Time,Latitude,Longitude,Altitude,North Velocity,East Velocity,Down Velocity,Roll,Pitch,Heading,Wander Angle,Track Angle,Speed,Angular Rate Longitudinal Axis,Angular Rate Transverse Axis,"
                       "Angular Rate Down Axis,Longitudinal Acceleration,Transverse Acceleration,Down Acceleration,Alignment Status\n";
    }
    void handleMsg(const lcm::ReceiveBuffer* rbuf,const std::string& chan) override;
    void handleMsg(const lcm::LogEvent* rbuf,const std::string& chan) override;
    void handle(lcmOPAL2::ins_data_msg_t& data) override;



protected:
    QString handleCSV(lcmOPAL2::ins_data_msg_t& msg) override;
    QString handleLog(lcmOPAL2::ins_data_msg_t& msg) override;
    void handleMod(lcmOPAL2::ins_data_msg_t& msg) override;
};

#endif // CHANNELHANDLER_H
