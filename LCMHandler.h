#ifndef LCMHANDLER
#define LCMHANDLER
#include <QString>
#include <QObject>
#include <QVector>
#include <QFile>
#include "channelhandler.h"


namespace lcm{class LCM;class ReceiveBuffer;class LogEvent;class LogFile;}
class LCMHandler: public QObject
{
    Q_OBJECT
public:
    enum ModificationType {
        FeetToMeter,
        None
    };
    LCMHandler(lcm::LCM* lcm);
    ~LCMHandler();
    QVector<ChannelHandler<void*>*>* channels(){
        return &m_channels;
    }
    QVector<ChannelHandler<void*>*>* activeChannels(){
        return &m_activeChannels;
    }
    void toggleMod(ModificationType mod);
    QVector<ModificationType> mods();
    void logEvent(lcm::LogEvent &event);
    void logCSV(QString &line,QString &channel);
    void logConsole(QString &str);
    QString prepareFiles(QString path, QVector<ChannelHandler<void*>*> &channels);

private:
    lcm::LCM* m_lcm;
    QVector<ModificationType> m_mods;
    QString m_outputPath;
    QVector<QFile*> m_csvFiles;
    lcm::LogFile* m_logFile;
    QVector<ChannelHandler<void*>*> m_channels;
    QVector<ChannelHandler<void*>*> m_activeChannels;

public slots:
    void handle();
signals:
    void logReady(QString* log);
};
#endif
