QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    LCMHandler.cpp \
    channelhandler.cpp \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    LCMHandler.h \
    channelhandler.h \
    lcmOPAL2/add_transform_msg_t.hpp \
    lcmOPAL2/header_t.hpp \
    lcmOPAL2/nav_data_msg_t.hpp \
    lcmOPAL2/nav_data_t.hpp \
    lcmOPAL2/point_data_msg_t.hpp \
    lcmOPAL2/point_data_raw_msg_t.hpp \
    lcmOPAL2/point_data_t.hpp \
    lcmOPAL2/point_data_t_raw.hpp \
    lcmOPAL2/point_t.hpp \
    lcmOPAL2/raw_point_t.hpp \
    lcmOPAL2/transform_msg_t.hpp \
    lcmOPAL2/transform_request_t.hpp \
    lcmOPAL2/transform_response_t.hpp \
    lcmOPAL2/transform_t.hpp \
    mainwindow.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

unix|win32: LIBS += -L$$PWD/'../../../../Program Files (x86)/lcm/lib/' -llcm

INCLUDEPATH += $$PWD/'../../../../Program Files (x86)/lcm/include'
DEPENDPATH += $$PWD/'../../../../Program Files (x86)/lcm/include'

DISTFILES += \
    header_t.lcm \
    nav_data_t.lcm
